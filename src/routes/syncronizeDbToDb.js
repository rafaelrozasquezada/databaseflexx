
import S from 'fluent-json-schema'
import authentificator from 'authenticator'
// u5qd 74x4 gwhl zxfa fe6x 2ays 5r3l wl7h

/**
 * Encapsulates the routes
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
export default async function routes (fastify, options) {
  const bodyJsonSchema = S.object()
                        .prop('totp',S.string().minLength(6).maxLength(6))
                        .prop('database', S.string().required())
                        .prop('collection', S.string().required())
                        .prop('action',S.enum(['EDIT','CREATE','DELETE']).required())
                        .prop('data',S.object().prop('id')/* .required() */)

  const schema = {
    body:bodyJsonSchema,
    //params:paramsSchema,
    //headers
    //query
  }

  fastify.route({
    method:'POST',
    schema,
    url:'/sync',
   /*  onRequest:async function(request, reply){
      console.log(request.body.totp);
    }, */
    //onResponse:options.onResponse,
    preHandler:async function(request, reply){
      console.log(request.body.totp);
      if(request.body.totp){
        console.log(authentificator.verifyToken('u5qd 74x4 gwhl zxfa fe6x 2ays 5r3l wl7h',request.body.totp))
      }
    },
    handler:async function ( request, reply ){
      console.log(request.body)
    }
  })
}
