
import find from "../controllers/find.js";
import findOne from '../controllers/findOne.js'
import create from "../controllers/create.js";
import { deleteMany, schema, createMany } from '../controllers/Many.js'
import edit from '../controllers/edit.js'
import deletController from '../controllers/delete.js'
import solo, { schema as soloSchema } from '../controllers/Solo.js'
/**
 * Encapsulates the routes
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
export default async function routes (fastify, options) {

  fastify.route({
    method:'GET',
    url:`/${options.prefix}/:database/:collection`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:find
  })

  fastify.route({
    method:'GET',
    url:`/${options.prefix}/:database/:collection/:id`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:findOne
  })

  fastify.route({
    method:'DELETE',
    schema,
    url:`/${options.prefix}/many/:database/:collection`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:deleteMany
  })

  fastify.route({
    method:'POST',
    schema,
    url:`/${options.prefix}/many/:database/:collection`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:createMany
  })

  fastify.route({
    method:'POST',
    schema: soloSchema,
    url:`/${options.prefix}/solo/:database/:collection`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:solo
  })

  fastify.route({
    method:'POST',
    url:`/${options.prefix}/:database/:collection`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:create
  })

  

  fastify.route({
    method:'PUT',
    url:`/${options.prefix}/:database/:collection/:id`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:edit
  })

  fastify.route({
    method:'DELETE',
    url:`/${options.prefix}/:database/:collection/:id`,
    onRequest:options.onRequest,
    onResponse:options.onResponse,
    preHandler:options.preHandler,
    handler:deletController
  })
}
