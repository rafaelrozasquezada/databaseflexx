import { permisoAdmin } from '../plugins/permision.js'
import { open } from 'lmdb'
import bearer from '@fastify/bearer-auth'
import databasesConfig from '../config/databases.json'

export default async function authenticatedContext(childServer, opts) {
  childServer.register(bearer, { keys: ['admin'] });

  childServer.route({
    path: '/create/:database/:rol/:app',
    method: 'POST',
    async handler(request, response) {
      const { database, rol: rolName, app, isAdmin } = request.params;
      console.log({databasesConfig})
      return {}
      if (!database) return response.code(400).send({ message: 'No se identifico la base de dato' });
      if (!this.idb['__config']) return response.code(400).send({ message: 'No se inicio la base de dato' });
      const config = this.idb['__config'];

      const dbTokens = await config.openDB('tokens');
      const crypto = await import('crypto');
      const claveSecreta = crypto.randomBytes(32).toString('hex');
      const tkn = `${this.uid()}::${rolName}::${app}`;
      dbTokens.put(`${claveSecreta}`, tkn);
      // token "aaaa"
      const idrol = tkn.split('::')[1];

      const dbRoles = await config.openDB('roles');
      await dbRoles.put(idrol, { 
        isAdmin, 
        nombre: rolName, 
        database, 
        actions: { leer: 1, escribir: 1, borrar: 1, editar: 1 }, 
        collections: { coll: 1, productos: 1, config: 1 } })
      const rol = await dbRoles.get(idrol);
      return { rol, tkn, claveSecreta }
    }
  }, opts)

  childServer.route({
    path: '/do',
    method: 'GET',
    handler(request, response) {
      return { hello: 'word' }
    }
  }, opts)
}