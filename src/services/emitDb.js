import { EventEmitter } from 'events';
import { open } from 'lmdb'
const path = './databases/queque'

class SyncDb extends EventEmitter {
  constructor( database, collection ){
    this.database = database
    this.collection = collection
    this.db = open(path,{
      dupSort:true,
      encoding:'ordered-binary'
    })
  }
  async create(data){
    this.db.put( `${this.database}::${this.collection}`, data )
    await this.db.flushed
    this.emit('webhook',`${this.database}::${this.collection}`, data);
  }

  async enviarQueue( key ){
    if(!this.db.doesExist(key)) return Promise.resolve(true);
    for (const value of this.db.getValues(key)) {
      
    }
    const queue = []
  }

  async getQueue(){

  }
}