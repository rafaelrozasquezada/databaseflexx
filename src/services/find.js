import qs from 'qs';
import populate from './populate.js'
import search from './search.js';


export default async function ( database, collection, query ){
  const quer = qs.parse(query);
  let { datos, meta } = search( collection, quer )
  //console.log(meta)
  quer['_llenar'] && (datos = await populate(quer['_llenar'], datos, database ));

  return { datos, meta }
}