import { where, gt, __, lt, gte, lte, equals,includes, type, toLower } from 'ramda';

const WS = {
  gt: 1,
  gte: 1,
  lt: 1,
  lte: 1,
  eq: 1,
  qs: 1,
}

const IGNORE = {
  _limit:1,
  _llenar:1,
  '_sort:DESC':1,
  '_sort:ASC':1,
  _desde:1,
  _hasta:1,
  _next:1
}

function tipe(consulta, valor) {
  let fn

  switch (consulta) {
    case 'gt':
      fn = gt(__, valor)
      break;
    case 'lt':
      fn = lt(__, valor)
      break;
    case 'gte':
      fn = gte(__, valor)
      break;
    case 'lte':
      fn = lte(__, valor)
      break;
    case 'lte':
      fn = lte(__, valor)
    break;
    case 'contains':
      if(type(valor) == 'String'){
          fn = (consultar)=> includes( toLower(valor), toLower(consultar) )
      }else {
        fn = (consultar)=> includes( valor, consultar )
      }
    break;
    case 'eq':
    default:
      fn = equals(valor)
    break;
  }

  return fn || null
}

/**
 * 
 * @param {Object[]} collection 
 * @param {object} query 
 * @returns {{ datos: Object[], meta:Object}} 
 */
export default function ( collection, query ) {
  let pred = null;
  let ws = {}
  try {
    for (const key in query) {
      if (IGNORE[key]) continue
      if (Object.hasOwnProperty.call(query, key)) {
        const element = query[key];
        const [ name, consulta ] = key.split('_');
        //console.log({name, consulta, element })
        ws[name] = tipe( consulta || 'eq', element )
        //console.log({ws})
      }
    }

    pred = where(ws)
  } catch (error) {
    console.trace(error)
  }
  const _limit = Number(query['_limit']);
  const limit = isNaN(_limit) ? 10 : Math.min(Number(_limit) , 100 );
  const datos = [];
  const nextPage = isNaN(query['_next']) ? 0 : Number(query['_next']);
  const _start = isNaN(query['_desde']) ? 0 : Number(query['_desde']);

  let totalcollection = 0;
  const optionIterate = {
    _start,
    _end : isNaN(query['_hasta']) ? Infinity : Number(query['_hasta']),
    offset : isNaN(query['_next']) ? 0 : Number(query['_next']),
  }
  let _desde = isNaN(query['_desde']) ? 0 : Number(query['_desde']);
  let _hasta = 0;
  let _next = isNaN(query['_next']) ? 0 : Number(query['_next']);;
  
  let count = 0;
  let cuantos = 0;

  for (const { key, value } of collection.getRange(optionIterate)) {
    totalcollection++
    if (pred) {
      if (pred(value)) {
        !_desde && ( _desde = totalcollection );
        _hasta = totalcollection
        count++
        if (cuantos >= limit){
          if(nextPage) break
          continue
        } 
        if(nextPage) _next++
        else _next = Math.min( totalcollection, limit )
        
        const object = { id: key, ...value }
        datos.push(object);
        cuantos++
      }
    } else datos.push(object)
  }
  return { datos, meta:{ _desde, _hasta, count, _next, porPage:limit }}
}