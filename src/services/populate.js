import { is, type } from 'ramda';
import { isValidUid } from '../utils/uuid.js';
import { open } from 'lmdb';

export default async function llenar(_llenar, datos, database) {
  if (!_llenar) return datos

  const collections = []
  const tiene = []

  if (is(String, _llenar)) {
    collections.push(open(database.db_path+_llenar))
    tiene.push(_llenar)
  } else if (is(Object, _llenar)) {

    if (type(_llenar) === 'Array') {

      for (const item of _llenar) {

        if (type(item) == 'String') {

          collections.push(open(database.db_path+item))
          tiene.push(item)
        } else if (is(Object, item)) {

          //TODO: 
          if (type(item) == 'Array') {

            for (const it of item) {

              if (type(it) === 'String') {
                collections.push(it)
              }

            }
          }
        }
      }
    } else {

    }
  }

  const cuantos = tiene.length
  if (!cuantos) return datos
  const populates = []

  for (let idatos = 0, datosL = datos.length; idatos < datosL; idatos++) {
    const entity = datos[idatos];

    for (let index = 0; index < cuantos; index++) {
      let relacion = tiene[index];

      if (!entity[relacion]) {
        const largo = String(relacion).length
        if (String(relacion).endsWith('s')) {
          relacion = String(relacion).slice(0, largo - 1)
        }
        if (!entity[relacion]){
          populates.push(entity)
          continue
        }
      }

      if (String(relacion).endsWith('s')) {
        if (type(entity[relacion]) !== 'Array'){
          populates.push(entity)
          continue
        }
          for (let i = 0,relcu = entity[relacion].length; i < relcu; i++) {
            const relacionId = entity[relacion][i];
            if(!isValidUid(relacionId)) continue
            entity[relacion][i] = await collections[index].get(relacionId)
          }
      } else {
        const id = entity[relacion]
        if (!isValidUid(id)){
          populates.push(entity)
          continue
        }
        entity[relacion] = await collections[index].get(id);
      }
      populates.push(entity)
    }
  }

  collections.forEach(databaseOpen => (databaseOpen.close()))

  return populates
}