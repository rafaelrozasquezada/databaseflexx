import Fastify from 'fastify'
import lmdbPlugin from './plugins/connectorDb.js'
import localbase from './plugins/localbase.js'
import admin from './routes/admin.js'
import permision from './plugins/permision.js'
import uuid from './plugins/uuid.js'

const fastify = Fastify({
  logger: true
})

fastify.register(uuid);
fastify.register(lmdbPlugin) //open database
fastify.register(permision)
fastify.register( admin,{  prefix:'/admin'} );
fastify.register( localbase, { prefix:'api' } ) //register rutas para base de datos authentificado
//fastify.register(routes);

const start = async ()=>{
  try {
    console.log('MEMORY: ',Math.round(process.memoryUsage().heapUsed/1024/1024))
    await fastify.listen({ port:process.env.port || 3000 , host:'localhost'})
  } catch (error) {
    console.trace(error)
    fastify.log.error(error)
    process.exit(1)
  }
}

start()