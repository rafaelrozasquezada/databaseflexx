import { mergeRight, omit } from 'ramda'
export default async (request, reply) => {
  const id = request.params.id
  const datos = omit( ['id','createdAt','updatedAt'] , request.body )
  datos.updatedAt = new Date()
  request.collection.transaction(async () => {
    const entity = await request.collection.get(id);
    if(entity){
      await request.collection.put(id, mergeRight( entity, datos ));
    }
  });
  await request.collection.flushed
  return await request.collection.get(id);
}