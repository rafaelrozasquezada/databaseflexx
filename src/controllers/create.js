export default async function(request, reply){  
  const id = this.uid()
  const now = new Date()
  const data = {
    id,
    ...request.body,
    createdAt:now,
    updatedAt:now
  }
  request.collection.transaction( async ()=> {
    await request.collection.put(id,data);
  })
  await request.collection.flushed
  return { data }
}