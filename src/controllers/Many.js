import S from 'fluent-json-schema';

const bodyJsonSchema = S.object()
  .prop('data', S.array()/* .required() */);

const paramsSchema = S.object().prop('database', S.string().required())
  .prop('collection', S.string().required());

export const schema = {
  body: bodyJsonSchema,
  params: paramsSchema,
  //headers
  //query
}
export async function createMany (request, reply) {
  const { data } = request.body;

  request.collection.transaction(async () => {
    const cuantos = data.length
    for (let index = 0; index < cuantos; index++) {
        const body = data[index];
        const id = this.uid()
        const now = new Date()
        const newDoc = {
          id,
          ...body,
          createdAt: now,
          updatedAt: now
        }
        await request.collection.put(id, newDoc);
    }
  })
  await request.collection.flushed
  return {ok:1}
}

export async function deleteMany (request, reply) {
  const { data } = request.body;
  request.collection.transaction(async () => {
    const cuantos = data.length
    for (let index = 0; index < cuantos; index++) {
      await request.collection.remove(data[index]);
    }
  })
  await request.collection.flushed
  return {ok:1}
}