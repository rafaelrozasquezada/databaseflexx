export default  async (request, reply) => {
  const entity = await request.collection.get(request.params.id)
  return entity || null
}