import S from 'fluent-json-schema';
import { project } from 'ramda'
import search from '../services/search.js';
const paramsSchema = S.object().prop('database', S.string().required()).prop('collection', S.string().required());
const bodySchema = S.object().prop('solo').prop('map',S.boolean().default(false)).required(['solo'])
export const schema = {
  params: paramsSchema,
  body: bodySchema
}
export default async function (request, reply) {
  const { solo, map } = request.body;
  if(!Array.isArray(solo)) throw Error('solo no es array')
  if(!solo.length) throw Error('No se ingresaron key en "solo" para retornar valores')
  const { datos } = search(request.collection, request.query);
  if(map && solo.length == 1){
    return datos.map(e => e[solo[0]])
  }
  return project(solo, datos)
}