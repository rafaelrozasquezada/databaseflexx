import find from "../services/find.js"

export default  async function (request, reply){
  //console.log('MEMORY: ',process.memoryUsage().heapUsed/1024/1024)
  return await find( request.db, request.collection, request.query );
}