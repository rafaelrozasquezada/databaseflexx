export default async (request, reply) => {
  const entity = await request.collection.remove(request.params.id)
  return { ok:entity? 1:0 }
}