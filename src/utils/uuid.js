import { v1, validate } from 'uuid';

export function formatearCadenaUidToUuid(cadena) {
  // Verifica si la cadena de entrada tiene la longitud esperada
  if (cadena.length === 32) {
    // Reorganiza la cadena en el nuevo formato
    const nuevaCadena =
      cadena.substring(6, 14) +
      '-' +
      cadena.substring(4, 8) +
      '-' +
      cadena.substring(0, 4) +
      '-' +
      cadena.substring(16, 20) +
      '-' +
      cadena.substring(20);

    return nuevaCadena;
  } else {
    // Devuelve un mensaje de error si la cadena no tiene la longitud esperada
    return "La cadena de entrada no tiene la longitud esperada (32 caracteres).";
  }
}

export function uid(){
  const unordered = v1();
  return unordered.substring(14,18) + unordered.substring(9,13) + unordered.substring(0,8) + unordered.substring(19,23) + unordered.substring(24, unordered.length);
}

export function isValidUid(uid){
  return /^[0-9a-f]{32}$/i.test(uid)
}

export function validateUuid(uid){
  return validate(formatearCadenaUidToUuid(uid));
}