import { open } from 'lmdb'
import fp from 'fastify-plugin'

/**
 * Encapsulates the coneccion database
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
function lmdbPlugins(fastify, opts, done) {
  const path = './databases/'

  !opts && (opts = {})

  if (!fastify.hasDecorator('pathdb')) {
    fastify.decorate('pathdb', path + 'other/')
  }

  if (!fastify.hasDecorator('idb')) {
    fastify.decorate('idb', {})
  }

  if (!fastify.idb['__config']) {
    fastify.idb['__config'] = open(path + 'internal/config');
  }

  fastify.addHook('onClose', (instance, done) => {
    instance.idb.close(done)
  })

  done()
}

export default fp(lmdbPlugins, { name: 'fatify-lmdb' })