const actions = { GET:'leer',POST:'escribir',PUT:'editar',DELETE:'borrar'}

async function getPermisionDatabases( token, request, reply ){

  const {database:db, collection } = request.params

  if(!db || !collection ) return reply.code(404).send({message:'Not found'})

  //await dbRoles.put(idrol,{isAdmin:true,nombre:'admin',database:'local',actions:{leer:1,escribir:1,borrar:1,editar:1},collections:{coll:1,productos:1,config:1}})
  const rol = await getRol.call(this,token)

  const { database, collections, actions:userActions, isAdmin } = rol;

  if( database !== db || !collections[collection] || !userActions[actions[request.method]]){
    throw Error('unauthorized');
  }

  request.isAdmin = !!isAdmin

  return true
}

async function getRol( token ){
  if(this.idb && !this.idb['__config']) throw Error('internal error')
  const dbConfig = this.idb['__config'];
    
    const dbTokens = await dbConfig.openDB('tokens');

    // token "aaaa"
    const tkn = await dbTokens.get(token);
    dbTokens.close();
  
    if(!tkn) throw Error('unauthorized');

    const idrol = tkn.split('::')[1];
  
    const dbRoles = await dbConfig.openDB('roles');
    //await dbRoles.put(idrol,{isAdmin:true,nombre:'admin',database:'local',actions:{leer:1,escribir:1,borrar:1,editar:1},collections:{coll:1,productos:1,config:1}})
    const rol = await dbRoles.get(idrol);

    console.log({rol})
  
    if(!rol) throw Error('unauthorized');

    return rol
}

async function isAdmin( token, request ){
    const rol = await getRol.call(this,token);
    const { isAdmin } = rol;
    request.isAdmin = !!isAdmin
    return !!isAdmin
}

async function getToken(request){
    const { authorization } = request.raw.headers;
      const { token } = request.query;
      delete request.query.token

      if(authorization){
        const [ bearer, token ] = authorization.split(' ');
        if(bearer !== 'Bearer') throw Error('format token invalid')
        return token.trim()
      }

      if(!token) throw Error('unauthorized')
      
      return token
}

export async function permisoSupcripcion(request, reply ){
  try {
    const token = await getToken(request);
    await getPermisionDatabases.call(this, token, request, reply);
  } catch (error) {
    return reply.code(401).send({message:error.message})
  }
}

export async function permisoAdmin(request, reply ){
  try {
    const token = await getToken(request );
    const isAdm = await isAdmin(token, request);
    if(!isAdm) reply.code(403).send({ message:'No permitido'})
  } catch (error) {
    return reply.code(401).send({message:error.message})
  }
}

/**
 * Verificacion de permisos
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options
 */
export default function permision (fastify, opts, done) {

  if (!fastify.hasDecorator('idb')) {
    return done(Error('No inicializada la base de datos'))
  }

  if(!fastify.idb['__config']){
    return done(new Error('No inicializada la base de datos'))
  }

  if(!fastify.hasRequestDecorator('isAdmin')){
    fastify.decorateRequest('isAdmin',null)
  }
  console.log("INICIANDO PERMISION")

  done()
}

//export default fp(permision, { name: 'permision'})