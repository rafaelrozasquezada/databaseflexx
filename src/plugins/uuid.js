import fp from 'fastify-plugin';
import { uid } from '../utils/uuid.js'

/**
 * Encapsulates the ordened uuid
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
function uidPlugins (fastify, opts, done) {

  if(!fastify.hasDecorator('uid')){
    fastify.decorate('uid',uid)
  }
  done()
}

export default fp(uidPlugins, { name: 'fatify-uid'})