import { open } from 'lmdb'
import fp from 'fastify-plugin'
import routes from '../routes/database.js'
import { permisoSupcripcion } from './permision.js'
import sync from '../routes/syncronizeDbToDb.js'

/**
 * Encapsulates the coneccion database
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
function localbasePlugins (fastify, opts, done) {
  const TIME_OUT = 1000 * 60 * 5;
  
  if(!fastify.hasDecorator('db')){
    fastify.decorate('db',{})
  }

  if(!fastify.hasDecorator('collection')){
    fastify.decorate('collection',{})
  }

  opts.onRequest = permisoSupcripcion

  opts.preHandler = async function (request, reply) {
    const { database, collection } = request.params;
    if( database && collection ){
      const path = `${database}/${collection}`

      if(!fastify.db[database]){
        console.log('OPEN DATABASE: ',database, collection)
        fastify.db[database] = {
          collection_path: fastify.pathdb+path,
          db_path: fastify.pathdb+`${database}/`
        };
      }
  
      if(!fastify.collection[collection]){
        fastify.collection[collection] = open(fastify.pathdb+path);
        //fastify.collection[collection] = fastify.db[database].openDB(collection);
      }
      
      if('timeOut' in fastify.collection[collection]){
        clearTimeout(fastify.collection[collection].timeOut)
        delete fastify.collection[collection].timeOut
      }
      request.db = fastify.db[database]
      request.collection = fastify.collection[collection]
    }
  }
  opts.onResponse = async (request, reply) => {
    const { database, collection } = request.params;

    if(fastify.collection[collection] && !fastify.collection[collection].timeOut){
      fastify.collection[collection].timeOut = setTimeout(()=>{
        try {
          console.log('CLOSE DATABASE: ',database, collection)
          fastify.collection[collection].close();
          delete fastify.db[database];
          delete fastify.collection[collection];
        } catch (error) {
          console.trace(error)
        }
      },TIME_OUT)
    }
  }

  routes( fastify, opts )

  sync( fastify )
  

 /*  fastify.addHook('preHandler',async function (request, reply) {
    const { database, collection } = request.params;
    if(database && collection ){
      if(!fastify.db[database]){
        console.log('OPEN DATABASE: ',database, collection)
        fastify.db[database] = open(fastify.pathdb+database);
      }
  
      if(!fastify.collection[collection]){
        fastify.collection[collection] = fastify.db[database].openDB(collection);
      }
      
      if('timeOut' in fastify.db[database]){
        clearTimeout(fastify.db[database].timeOut)
        delete fastify.db[database].timeOut
      }
      request.db = fastify.db[database]
      request.collection = fastify.collection[collection]
    }
  }) */

/*   fastify.addHook('onResponse', async (request, reply) => {
    const { database, collection } = request.params;

    if(fastify.db[database] && !fastify.db[database].timeOut){
      fastify.db[database].timeOut = setTimeout(()=>{
        try {
          console.log('CLOSE DATABASE: ',database, collection)
          fastify.collection[collection].close();
          fastify.db[database].close();
          delete fastify.db[database]
          delete fastify.collection[collection]
        } catch (error) {
          console.trace(error)
        }
      },TIME_OUT)
    }
  })
 */
  
  fastify.addHook('onClose', (instance, done) => {
    console.log("CERRANDO");
    for (const iterator of instance.collection) {
      iterator.close()
    }
    delete instance.collection
    done()
  })
  done()
}
export default fp(localbasePlugins, { name: 'fatify-localbase'})